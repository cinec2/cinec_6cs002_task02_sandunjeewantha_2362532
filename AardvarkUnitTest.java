import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class AardvarkUnitTest {

	@Test
	void test() {
		Aardvark aardvark = new Aardvark();
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		
		//printMainMenu Test
		System.setOut(new PrintStream(outContent));
		String expectedOutput = "\n=========\nMain menu\n=========\n1) Play\n2) View high scores\n3) View rules\n0) Quit\n"
				.replaceAll("\\n|\\r\\n", System.getProperty("line.separator"));
		aardvark.printMainMenu();
		String actualOutput = outContent.toString();
		assertEquals(expectedOutput, actualOutput);

		//printPlayMenu Test
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		expectedOutput = "\n=========\nPlay menu\n=========\n1) Print the grid\n2) Print the box\n3) Print the dominos\n4) Place a domino\n5) Unplace a domino\n6) Get some assistance\n7) Check your score\n0) Given up\nWhat do you want to do null?\n"
				.replaceAll("\\n|\\r\\n", System.getProperty("line.separator"));
		aardvark.printPlayMenu();
		actualOutput = outContent.toString();
		assertEquals(expectedOutput, actualOutput);

		//printSelectDifficulty Test
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		expectedOutput = "\n=================\nSelect difficulty\n=================\n1) Simples\n2) Not-so-simples\n3) Super-duper-shuffled\n"
				.replaceAll("\\n|\\r\\n", System.getProperty("line.separator"));
		aardvark.printSelectDifficulty();
		actualOutput = outContent.toString();
		assertEquals(expectedOutput, actualOutput);
	}

}
